Plug 'reyu/vim-virtualenv'
Plug 'vim-scripts/indentpython.vim'

let g:python_highlight_all=1
setlocal tabstop=4
setlocal softtabstop=4
setlocal shiftwidth=4
setlocal textwidth=120
"" Python venv
:fun ReturnVirtualEnvs(A,L,P)
:    return system("ls -d /home/tvallois/.local/share/virtualenvs/* \| cut -d'/' -f7") ""change the directory path to point to your virtualenvs
:endfun

"" changing virtualenv should restart ycmserver
:command -nargs=+ -complete=custom,ReturnVirtualEnvs Venv :VirtualEnvActivate <args> | YcmRestartServer
let g:ycm_python_binary_path = 'python'
