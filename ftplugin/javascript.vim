Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx'

setlocal tabstop=2 
setlocal softtabstop=2 
setlocal shiftwidth=2 
setlocal textwidth=120

